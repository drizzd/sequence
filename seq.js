class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    add(other) {
        return new Point(
            this.x + other.x,
            this.y + other.y
        );
    }

    subtract(other) {
        return new Point(
            this.x - other.x,
            this.y - other.y
        );
    }
}

class Svg {
    constructor(root) {
        this.root = root;
        this.selectedElement = null;
        this.dragOffset = null;
        this.zoomLevel = 0;
        this.nodes = [];

        this.onWheel = (event) => {
            event.preventDefault();
            console.log('onWheel', event.deltaY);
            this.zoomLevel += event.deltaY;
            let zoom = Math.pow(1.3, this.zoomLevel);
            for (let node of this.nodes) {
                node.zoom = zoom;
            }
        }
        this.root.addEventListener("wheel", this.onWheel);
        
        this.onMouseDown = (event) => {
            if (event.target.getAttribute("draggable") == 'true') {
                this.selectedElement = event.target;
                this.dragOffset = this.getMousePosition(event).subtract(this.getSelectedElementPosition());
            }
        };
        this.root.addEventListener("mousedown", this.onMouseDown);
        
        this.onMouseMove = (event) => {
            if (this.selectedElement) {
                event.preventDefault();
                let newPosition = this.getMousePosition(event).subtract(this.dragOffset);
                this.selectedElement.dispatchEvent(new CustomEvent("drag", {
                    detail: {
                        position: newPosition
                    }
                }));
            }
        };
        this.root.addEventListener("mousemove", this.onMouseMove);
    
        this.onMouseUp = (event) => {
            this.selectedElement = null;
            this.dragOffset = null;
        };
        this.root.addEventListener("mouseup", this.onMouseUp);
    }

    getSelectedElementPosition() {
        return this.getElementPosition(this.selectedElement);
    }

    setSelectedElementPosition(position) {
        this.selectedElement.setAttributeNS(null, "x", position.x);
        this.selectedElement.setAttributeNS(null, "y", position.y);
    }

    getMousePosition(event) {
        const ctm = this.root.getScreenCTM();
        return new Point(
            (event.clientX - ctm.e) / ctm.a,
            (event.clientY - ctm.f) / ctm.d
        );
    }

    getElementPosition(element) {
        return new Point(
            parseFloat(element.getAttribute("x")),
            parseFloat(element.getAttribute("y"))
        );
    }

    add(node) {
        this.nodes.push(node);
        this.root.appendChild(node.element);
    }
}

class Flow {
    constructor(svg) {
        this.nodes = [];
        this.svg = svg;
    }

    add(node) {
        node.addEventListener("drag", (event) => this.onDrag(node, event));
        this.svg.add(node);
        this.nodes.push(node);
    }

    onDrag(node, event) {
        node.start = event.detail.position.x;

        let nodesWithStart = [];
        let currentEnd = 0;
        for (let next of this.nodes) {
            nodesWithStart.push({ node: next, start: currentEnd });
            currentEnd += next.duration;
        }
        let offset = node.start - nodesWithStart.find(e => e.node === node).start;

        let index = this.nodes.indexOf(node);
        let predecessors = this.nodes.slice(0, index);
        for (let predecessor of nodesWithStart.filter(e => predecessors.includes(e.node))) {
            const maxStart = predecessor.start + offset;
            if (predecessor.node.start > maxStart) {
                predecessor.node.start = maxStart;
            }
        }

        let successors = this.nodes.slice(index + 1);
        for (let successor of nodesWithStart.filter(e => successors.includes(e.node))) {
            const minStart = successor.start + offset;
            if (successor.node.start < minStart) {
                successor.node.start = minStart;
            }
        }
    }
}

class Node {
    constructor(start, duration, group) {
        const height = 20;
        const xmlns = "http://www.w3.org/2000/svg";
        
        this._element = document.createElementNS(xmlns, "rect");
        this._element.setAttribute("y", 10 + 1.1*height*group);
        this._element.setAttribute("height", height);
        this._element.setAttribute("draggable", "true");
        this._element.setAttribute("fill", "lightblue");

        this.text = document.createElementNS(xmlns, "text");
        let fontSize = 10;
        this.text.setAttribute("y", 10 + 1.1*height*group + height/2 + fontSize/2);
        this.text.style['pointer-events'] = 'none';
        this.text.textContent = `X${group}`;
        this.g = document.createElementNS(xmlns, "g");
        this.g.appendChild(this._element);
        this.g.appendChild(this.text);

        this._start = start;
        this._duration = duration;
        this._zoom = 1;
        this.update();
    }

    get element() {
        return this.g;
    }

    get duration() {
        return this._duration;
    }

    set duration(duration) {
        this._duration = duration;
        this.update();
    }

    set zoom(zoom) {
        this._zoom = zoom;
        this.update();
    }

    update() {
        this._element.setAttribute('width', this._zoom * this._duration);
        this._element.setAttribute('x', this._zoom * this._start);
        this.text.setAttribute('x', this._zoom * this._start + 1);
    }

    get start() {
        return this._start;
    }

    set start(start) {
        this._start = start;
        this.update();
    }

    addEventListener(type, listener) {
        this._element.addEventListener(type, listener);
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let svgRoot = document.getElementById("svg");
    let svg = new Svg(svgRoot);

    let flow = new Flow(svg);
    flow.add(new Node(10, 80, 0));
    flow.add(new Node(50, 80, 1));
    flow.add(new Node(150, 40, 2));

    let flow2 = new Flow(svg);
    flow2.add(new Node(10, 80, 4));
    flow2.add(new Node(50, 80, 5));
    flow2.add(new Node(150, 40, 5));
});